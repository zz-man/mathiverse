---
layout: page
title: Home
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
# parent: index.md # will make this menu item a child of the index.md item
weight: 0 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
---

Willkommen im **MathIverse**!