---
layout: page
title: Pascal Prokoschoff
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
parent: personen # will make this menu item a child of the index.md item
# weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
---

Prof. Prokhoschoff ist guter Freund von ZZ-Man. Er ist derjenige, der die vier Lemmata zusammengesetzt hat. Prof. Prokhoschoff unterstütz ZZ-Man in seinem Bestreben, fehlerhafte Beweise zu korrigieren und Erstsemester zu verbessern. Auch trainiert er ZZ-Man auf ihm noch unbekannten Teilgebieten der Mathematik.