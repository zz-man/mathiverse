---
layout: page
title: Zaharias Zorn
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
parent: personen # will make this menu item a child of the index.md item
# weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
id: zz-man
---

### Inhalt
1. [Origins](#origins)
    1. [Kindheit](#kindheit)
    2. [Uni](#uni)
    3. [Becoming ZZ-Man](#becoming-zz-man)
    4. [Nemesis](#Nemesis)
2. [ZZ-Man](#zz-man)
    1. [Die Lemmata](#die-lemmata)
    2. [Fähigkeiten](#fähigkeiten)
    3. [Ausrüsting](#ausrüstung)
    4. [Kostüm](#kostüm)


**Deckname:** ZZ-Man

### Origins

#### Kindheit

Geboren wurde ZZ-Man mit dem bürgerlichen Namen Zaharias Zorn. Seine jüngere Schwester [Zarah Zorn](zarahzorn) und seine Eltern kamen bei einem Zugunglück ums Leben. Ihr Zug kollidierte mit einer Stopfmaschine. Die Firma, die die Bremsen des Zuges herstellt, hatte ein Verfahren für die Berechnung genutzt, von dem bewiesen ist, dass es in diesem speziellen Anwendungsfall nicht das richtige Ergebnis liefert.
Erst viel später, als ZZ-Man schon zu ZZ-Man geworden ist, erfährt ZZ, dass seine Schwester das Zugunglück überlebt hat. Als sie von ZZ-Mans Alter-Ego erfährt, wendet sie sich gegen ihn und versucht ihm als Direktorin Dirac seine Kräfte zu nehmen.

#### Uni

ZZ-Man arbeitet als Sekretär in der mathematischen Fakultät einer Universität. ZZ-Man ist bekannt dafür, dass er immer sehr korrekt gekleidet mit Fliege und Hosenträgern bei der Arbeit erscheint. Er hat häufig mit der gebieterischen Studiendekanin [Stephanie Schauder](stephanieschauder) zu kämpfen, die ihm das Leben schwer macht.

#### Becoming ZZ-Man

Nach einer Info-Veranstaltung für Abiturienten wischt ZZ-Man spät Abends die Tafel. In der Hand hält er einige Mathe-Bücher, die er als Anschauungsmaterial dabei hatte. Als er über ein loses Schauderabel der veralteten Beamer-Technik stolpert und in einer Tafelwasser-Lache landet, erhält er einen Schauderschlag. Er hält sich an der Tafel fest, die - weil alt - aber auf ihn fällt. Begraben unter der Tafel, auf der ein ausführlicher Beweis steht, die Mathebücher noch im Arm schickt, ihn der Schauder in die Bewusstlosigkeit.
Gefunden wird er am nächsten Morgen von [Professor Prokhoschoff](pascalprokoschoff) gefunden. ZZ-Man ist in vier Lemmata zerfallen. Daneben liegt ein grüner Umhang, der sich schützend vor die Lemmata stellt. Es ist die verwandelte Tafelt. Prokhoschoff trägt die Lemmata im Mantel aus dem Hörsaal und fügt unter großen Mühen die vier Lemmata zu ZZ-Man zusammen.
Prof. Prokhoschoff unterstütz ZZ-Man fortan in seinem Bestreben, fehlerhafte Beweise zu korrigieren und Erstsemester zu verbessern. Auch trainiert er ZZ-Man auf ihm noch unbekannten Teilgebieten der Mathematik.

#### Nemesis

Die Studiendekanin Professorin Stophanie Schauder kommt mit Hilfe von [Peter Putzer](peterputzer) bald auf ZZ-Mans Schliche und versucht, ZZ-Mans Entstehung nachzuahmen

### ZZ-Man

#### Die Lemmata

Bei Bedarf kann ZZ-Man in die vier Lemmata zerfallen. Sie sind die Äquivalenklassen des Quotientenraums ```ZZ-Man/~```. In zerfallener Form ist ZZ-Man allerdings angreifbarer.

Jedes Lemma hat eine Aussage/einen Special Move. Zerfällt ZZ-Man, können diese Aussagen besser anwenden.

| Lemma                     | Aussage/Special Move                                                                                                                                                                        |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Kontrapositions Kommander | Kehrt eine (korrekte) Aktion um und wirft sie auf den Gegner zurück.                                                                                                                        |
| Widerspruchs Annihilator  | Erkennt kleinste Logikfehler in gegnerische Logik, wickelt Gegner mit Widersprüchen ein und annihiliert ihn so.                                                                             |
| Induktions Initiator      | Lässt eine gegnerische Aktion sich (abzählbar!) unendlich oft wiederholen.                                                                                                                  |
| Ringschluss Restriktor    | Schränkt Gegner auf ein Gebiet ein, auf dem die Aussage nicht gilt. Kann das Gebiet mit einem Torus identifizieren, so dass Gegner sich im Kreis bewegt, wenn er das Gebiet verlassen will. |

#### Fähigkeiten

* **Ruf der Vernunft** Wenn ZZ-Man aus tiefster Kehle ***"Assert! Prove! Repeat!"*** schreit, bleibt seinen Feinden für einige Momente vor strikten Folgerungen die Luft weg.
* **Paranthesis Gun** ZZ-Man kann Klammern aus den beiden Bögen des Q's auf seiner Brust schießen und so seine Gegner fangen.

#### Ausrüstung

* **Kreitonengürtel**
  Hosenträger mit Steckplätzen für Kreide
* **Konvexe Hülle** Die Mantel gewordene Tafel, die ZZ-Man auf Schritt und Tritt begleitet und ihm stets sehr sorgfältig die Tafel reinigt
* **Monokel** mit dem man Aussagen leicht auf ihren Gehalt prüfen kann. Sehr nützlich gegen [den Zeitschinder](studenten#der-zeitschinder) 
* **Taschenuhr** *was kann die außer cool sein?*

#### Kostüm

* ∞-Symbol-Maske
* Kreitonengürtel
* Konvexe Hülle