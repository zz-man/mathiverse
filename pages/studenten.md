---
layout: page
title: Studenten
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
parent: personen # will make this menu item a child of the index.md item
# weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
---

Es gibt so'ne Studenten und solche Studenten und dann gibt's noch ganz andere und *das* sind die schlimmsten …

#### Der Zeitschinder

versucht stets seine Prüfungen zu bestehen, indem er die Fragen, geschickt als Aussagen formuliert, wiederholt.

#### Der Attest-Boy

zerstört den Tagesablauf jedes Prüfers indem er nicht zur Prüfung erscheint und anschließend ein wenig glaubhaftes Attest einreicht.

#### Der PO-Bender

engagiert sich in der Fachschaft, um die Prüfungsordnung zu seinen Gunsten zu ändern und so den Studientgang doch noch abschließen zu können.