---
layout: page
title: Ideen-Sammlung
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
# parent: index.md # will make this menu item a child of the index.md item
weight: 100 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
id: ideas
---

**Auf dieser Seite können Ideen gesammelt werden, die man bei Zeiten in die Geschichte einbringen möchte.**

## Schurken

* Ein Schurke, der mit einer Abbildung identifiziert werden kann, die in einen Nicht-Haussdorffraum-abbildet (bzw einen ohne ``$T_1$``-Eigenschauft). Weil dort keine Punkte getrennt werden können, kann das Opfer nicht fliehen.

* Der **Prokastonator** (z.B. Pafnuti P. nach Pafnuti Tschebyscheff). Er war (das haben laut eines britischen Geheimdienstberichtes ja schwedische Wissenschaftler herausgefunden) Mitglied der Abelschen Gruppe, einer Terrorzelle um Sofja Wassiljewna Kowalewskaja. Superkraft: Vllt. was mit dem Cantorschen Diagonalverfahren.
* Schatten von Neumann (Schurken mit Klasse)

## Diverses

* Die Picard-Lindelöf-Iteration in Verbindung mit Captain Picard aus Star Trek in einem Weltraum-Abenteuer von ZZ-Man. Außerdem: ``$\pi_k$``.
* Paranormale oder Paranoide Operatoren. Siehe [Wiki](https://de.wikipedia.org/wiki/Normaler_Operator#Verwandte_Begriffe)
* Leer-Leo in Anlehnung an der Lehr-Leo der TU BS
* Ägyptische Quadratschädeleule: Schutzpatron der austauschbaren Doktoranden (graduiert kommutative Personen), auch als Superkommutator bekannt
* Vampirameise, Hase und Pirat. Eventuell durch Zarahs Stochastologie von Schauders Tafel heraufbeschworene Kreide-Golems (Kreilems?)? Vervollständigung mittels Kreitonengürtel durch hinzufügen virtueller Punkte: Hasenohren, Augenklappe,...
* Zarah stochastisiert sich weihnachtlich
* Nuclear-Normen sind bestimmt für irgendetwas gut, insbesondere robust
* Räume auf einen Punkt zusammenschlagen (irgendwas aus der Topologie)
* Die Faust mit dem Gesicht identifizieren
