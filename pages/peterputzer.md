---
layout: page
title: Peter Putzer
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
parent: personen # will make this menu item a child of the index.md item
# weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
---

**Deckname:** Der Konvolute Kontradiktor

Peter Putzer ist eine Reinigungskraft an der Universität. Er kommt hinter ZZ-Mans Geheimnus und wendet sich an Prof. Stephanie Schauder. Er ist fortan ihr Handlanger. Prof. Schauder gibt ihm einen möglichst erschreckend klingenden aber eigentlich nichtssagenden Decknamen, da sie nicht mit einer Reinigungskraft gesehen werden will.