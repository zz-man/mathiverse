---
layout: page
title: Prof. Stephanie Schauder
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
parent: personen # will make this menu item a child of the index.md item
# weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
---

Studiendekanin der Mathematik, Chefin von ZZ.

Mit Hilfe von [Peter Putzer](peterputzer) kommt sie bald auf ZZ-Mans Schliche und versucht, ZZ-Mans Entstehung nachzuahmen.
Da sie das allerdings im Urbildraum (Büro) statt dem Bildraum (Hörsaal) tut, landet sie beim Deltanium statt beim Epsilonium.

Sie wird schnell zu ZZ-Mans Nemesis.