---
layout: page
title: MathIverse
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
# parent: index.md # will make this menu item a child of the index.md item
weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
id: mathiverse
---

## Epsilonium und Deltanium
Epsilonium und Deltanium sind Größen, über die die Kraft der Mathematik genutzt werden kann. Sie sind das Mana/die Macht/was auch immer des MathIverse.
Epsilonium ist dabei die "Helle Seite der Macht", Deltanium die "Dunkle". Sie sind es, die [ZZ-Man](zahariaszorn) bzw [Prof. Stephanie Schauder](stephanieschauder) ihre Kräfte verleihen.