---
layout: page
title: Personen
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
# parent: index.md # will make this menu item a child of the index.md item
weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
id: personen
---

### Familie Zorn

- [Zaharias Zorn](zahariaszorn)
- [Zarah Zorn](zarahzorn)

### ZZ-Mans Widersacher

- [Nemesis: Prof. Stephanie Schauder](stephanieschauder)
- [Der Konvolute Kontradiktor](peterputzer)
- [Directorin Dirac](zarahzorn)
- [PO-Bender](studenten#der-po-bender)
- [Zeitschinder](studenten#der-zeitschinder)
- [Attest-Boy](studenten#der-attest-boy)