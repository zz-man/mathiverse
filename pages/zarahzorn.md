---
layout: page
title: Zarah Zorn
# menu: Example Page # will use this as the menu item text instead of title, set to false to remove from menu
# tab: true # will open this page in a new tab
parent: personen # will make this menu item a child of the index.md item
# weight: 1 # smaller weights rise to the top of the menu
# mdl_colors: deep_orange-blue # override global color scheme for this page
# search: false # hides this page from search results
---

**Deckname:** Directorin Dirac

Zarah Zorn ist die Schwester von ZZ-Man. Sie hat als einzige das Zugunglück überlebt, wurde von Nomaden-Physikern vom Stamme der Phyareg großgezogen und ist nun Direktorin des Instituts für Stochastische Physik und ihre Vermittlung (IStoPhyV) an einer Dualen Hochschule.

Sie hat eine zwiegespaltene Meinung zu ZZ-Man und verbündet sich nur gegen starke Feinde mit ihm.